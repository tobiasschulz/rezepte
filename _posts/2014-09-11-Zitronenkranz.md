---
layout: default
title: Zitronenkranz
---

## Zitronenkranz

Rührteig für Kranzform (28cm):

- 375g Butter
- 375g Zucker
- 375g Mehl
- 5 Eier
- Schale von 2 Zitronen
- Saft von 1,5 Zitronen
2 gestrichene Teelöffel Backpulver

Rührteig herstellen, in gefettete Kranzform füllen und auf die untere Schiene in den **kalten** Backofen schieben.
Backzeit 60 min bei 180-200 Grad.

<a href="{{site.baseurl}}/img/zitronenkranz.jpg"><img alt="Zitronenkranz" src="{{site.baseurl}}/img/zitronenkranz.jpg" class="original_rezept" /></a>

