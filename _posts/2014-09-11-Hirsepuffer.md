---
layout: default
title: Hirsepuffer
---

## Hirsepuffer

- 2 bis 4 Eier
- Hirse (20 min in Gemüsebrühe kochen)
- Haferflocken
- Schnittlauch
- Pfeffer
- Salz
- Muskat

<a href="{{site.baseurl}}/img/hirsepuffer.jpg"><img alt="Hirsepuffer" src="{{site.baseurl}}/img/hirsepuffer.jpg" class="original_rezept" /></a>
