---
layout: default
title: Kartäuser Klöße (Tante Agnes)
---

## Kartäuser Klöße (Tante Agnes)

- 6-8 altbackene Semmel
- 0,5l Milch
- 2 Eier
- 40g Zucker
- 1 Vanillezucker
- Zitronenschale, Semmelmehl, Backfett

Semmel abreiben und halbieren. Milch mit Eigelb, Zucker, Vanillezucker und Zitronenscheibe verquirlen, Semmeln darin gut durchweichen lassen. Semmelhälften im verschlagenen Eiweiß und Semmelmehl wenden und in reichlich heißem Fett in der Pfanne von allen Seiten goldbraun braten. Nach belieben mit Zucker, Zimt und Vanillezucker bestreut auf heißer Platte anrichten.

<a href="{{site.baseurl}}/img/kartaeuser-kloesse-tante1.jpg"><img alt="Kartäuser Klöße 1" src="{{site.baseurl}}/img/kartaeuser-kloesse-tante1.jpg" class="original_rezept" /></a> <a href="{{site.baseurl}}/img/kartaeuser-kloesse-tante2.jpg"><img alt="Kartäuser Klöße 2" src="{{site.baseurl}}/img/kartaeuser-kloesse-tante2.jpg" class="original_rezept" /></a>

