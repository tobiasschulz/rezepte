---
layout: default
title: Tortenboden (Tante Agnes)
---

## Tortenboden (Tante Agnes)

- 75g Mehl
- 50g Speisestärke
- 100g Zucker
- 2 Eier
- 1 Löffel Backpulver
- 3 Esslöffel Wasser

**Backzeit:** ca. 25 min bei 160 Grad Heißluft.

<a href="{{site.baseurl}}/img/tortenboden-tante-agnes.jpg"><img alt="Tortenboden" src="{{site.baseurl}}/img/tortenboden-tante-agnes.jpg" class="original_rezept" /></a>

